(in-package #:ck3gen)

(defstruct run
  (start 0 :type (integer 0))
  (end 0 :type (integer 0)))

(defun extend-runs! (runs x)
  (if runs
      (loop
        :with runs-tagged := (cons :run runs)
        :for curr-runs :on runs
        :for previous-cell :on runs-tagged
        :for (run . runs-rest)
          := curr-runs
        :do
           (bind ((start (run-start run))
                  (end (run-end run)))
             (cond
               ((= (- start x)
                   1)
                (setf (run-start run)
                      x)
                (return (values runs t)))
               ((< x start)
                (setf (cdr previous-cell)
                      (cons (make-run :start x
                                      :end x)
                            curr-runs))
                (return (values (cdr runs-tagged)
                                t)))
               ((<= x end)
                (return (values runs nil)))
               ((= (- x end)
                   1)
                (if runs-rest
                    (bind (((next-run &rest next-runs-rest)
                            runs-rest))
                      (if (= (- (run-start next-run)
                                x)
                             1)
                          (setf (run-end run)
                                (run-end next-run)
                                (cdr curr-runs)
                                next-runs-rest)
                          (setf (run-end run)
                                x)))
                    (setf (run-end run)
                          x))
                (return (values runs t)))
               ((not runs-rest)
                (setf (cdr curr-runs)
                      (list (make-run :start x
                                      :end x)))
                (return (values runs t))))))
      (values (list (make-run :start x
                              :end x))
              t)))

(defstruct pixel-set
  (runs (make-hash-table :test #'eql)
   :type hash-table
   :read-only t)
  (size 0 :type (integer 0)))

(defmethod size ((collection pixel-set))
  (pixel-set-size collection))

(defun simple-add-pixel! (pixel-set x y)
  (bind ((runs-table (pixel-set-runs pixel-set))
         ((:values runs new?)
          (extend-runs! (gethash y runs-table)
                        x)))
    (setf (gethash y runs-table)
          runs)
    (when new?
      (incf (pixel-set-size pixel-set)))))

(defun simple-in-pixel-set? (pixel-set x y)
  (some (lambda (run)
          (<= (run-start run)
              x (run-end run)))
        (gethash y (pixel-set-runs pixel-set))))

(defun simple-adjacent-to-pixel-set? (pixel-set x y)
  (or (in-pixel-set? pixel-set (1- x) y)
      (in-pixel-set? pixel-set (1+ x) y)
      (in-pixel-set? pixel-set x      (1- y))
      (in-pixel-set? pixel-set x      (1+ y))))

(defmethod fset:lookup ((collection pixel-set)
                        (key list))
  (in-pixel-set? collection (first key)
                 (second key)))

(defmethod arb ((collection pixel-set))
  (with-hash-table-iterator (next (pixel-set-runs collection))
    (bind (((:values found? y runs)
            (next)))
      (values (and runs (list (run-start (first runs))
                              y))
              found?))))

(defun simple-remove-pixel! (pixel-set x y)
  (bind ((runs-table (pixel-set-runs pixel-set))
         (runs (gethash y runs-table)))
    (loop
      :for (current . rest)
        :on runs
      :with tagged-runs
        := (cons :runs runs)
      :for previous-cons
        :on tagged-runs
      :do
         (bind ((start (run-start current))
                (end (run-end current)))
           (cond
             ((< x start)
              (return nil))
             ((= x start)
              (decf (pixel-set-size pixel-set))
              (if (= start end)
                  (setf (cdr previous-cons)
                        rest
                        (gethash y runs-table)
                        (cdr tagged-runs))
                  (incf (run-start current)))
              (return t))
             ((< x end)
              (decf (pixel-set-size pixel-set))
              (setf (run-end current)
                    (1- x)
                    (cddr previous-cons)
                    (cons (make-run :start (1+ x)
                                    :end end)
                          rest)))
             ((= x end)
              (decf (pixel-set-size pixel-set))
              (decf (run-end current))
              (return t)))))))

(defun simple-pop-pixel! (pixel-set)
  (bind ((runs-table (pixel-set-runs pixel-set)))
    (with-hash-table-iterator (next runs-table)
      (bind (((:values found? y runs)
              (next)))
        (when found?
          (decf (pixel-set-size pixel-set))
          (bind (((run &rest other-runs)
                  runs)
                 (start (run-start run)))
            (if (= start (run-end run))
                (if (null other-runs)
                    (remhash y runs-table)
                    (pop (gethash y runs-table)))
                (incf (run-start run)))
            (list start y)))))))

(defun set<-pixel (x y)
  (make-pixel-set :runs (bind ((result (make-hash-table :test #'eql)))
                          (setf (gethash y result)
                                (list (make-run :start x
                                                :end x)))
                          result)
                  :size 1))

(defun pixel-set<-image (image)
  (with-image-bounds (y-max x-max)
                     image
    (make-pixel-set :size (* x-max y-max)
                    :runs (loop
                            :with runs := (make-hash-table :test #'eql)
                            :for y :below y-max
                            :do
                               (setf (gethash y runs)
                                     (list (make-run :start 0
                                                     :end x-max)))
                            :finally
                               (return runs)))))

(defun integer-ball (center radius)
  (bind ((ball (make-pixel-set))
         (runs-table (pixel-set-runs ball))
         (r² (expt radius 2))
         ((cx cy)
          center))
    (loop
      :for dy
        :from (floor (- radius))
          :to (ceiling radius)
      :do
         (bind ((dx² (- r² (expt dy 2))))
           (unless (minusp dx²)
             (bind ((dx (sqrt dx²))
                    (left (ceiling (- cx dx)))
                    (right (floor (+ cx dx))))
               (incf (pixel-set-size ball)
                     (1+ (- right left)))
               (setf (gethash (round (+ cy dy))
                              runs-table)
                     (list (make-run :start (max left 0)
                                     :end right)))))))
    ball))


(defstruct surfaced-pixel-set
  (content (make-pixel-set)
   :type pixel-set
   :read-only t)
  (surface (make-hash-table :test 'equal)
   :type hash-table
   :read-only t))

(defun unsurface (surfaced-pixel-set)
  (surfaced-pixel-set-content surfaced-pixel-set))

(defun is-surface-pixel? (surfaced-pixel-set x y)
  (gethash (list x y)
           (surfaced-pixel-set-surface surfaced-pixel-set)))

(defun surface-add-pixel! (surfaced-pixel-set x y)
  (bind ((ps (surfaced-pixel-set-content surfaced-pixel-set))
         (surface (surfaced-pixel-set-surface surfaced-pixel-set)))
    (add-pixel! ps x y)
    (remhash (list x y)
             surface)
    (do-neighbors x y (nx ny)
      (unless (in-pixel-set? ps nx ny)
        (setf (gethash (list nx ny)
                       surface)
              t)))))

(defun surface-remove-pixel! (surfaced-pixel-set x y)
  (bind ((ps (surfaced-pixel-set-content surfaced-pixel-set))
         (surface (surfaced-pixel-set-surface surfaced-pixel-set)))
    (remove-pixel! ps x y)
    (bind ((is-surface? nil))
      (do-neighbors x y (nx ny)
        (cond
          ((in-pixel-set? ps nx ny)
           (setf is-surface? t))
          ((gethash (list nx ny)
                    surface)
           (bind ((is-surface? nil))
             (do-neighbors nx ny (nnx nny)
               (when (in-pixel-set? ps nnx nny)
                 (setf is-surface? t)))
             (unless is-surface?
               (remhash (list nx ny)
                        surface))))))
      (when is-surface?
        (setf (gethash (list x y)
                       surface)
              t)))))

(defmethod size ((collection surfaced-pixel-set))
  (size (surfaced-pixel-set-content collection)))

(defmethod fset:lookup ((collection surfaced-pixel-set)
                        key)
  (@ (surfaced-pixel-set-content collection)
     key))

(defmethod arb ((collection surfaced-pixel-set))
  (arb (surfaced-pixel-set-content collection)))

(defun surfaced-set<-pixel (x y)
  (bind ((result (make-surfaced-pixel-set :content (set<-pixel x y)))
         (surface (surfaced-pixel-set-surface result)))
    (do-neighbors x y (nx ny)
      (when (and (>= nx 0)
                 (>= ny 0))
        (setf (gethash (list nx ny)
                       surface)
              t)))
    result))

(defgeneric add-pixel! (pixel-set x y)
  (:method ((pixel-set pixel-set)
            (x integer)
            (y integer))
    (simple-add-pixel! pixel-set x y))
  (:method ((pixel-set surfaced-pixel-set)
            (x integer)
            (y integer))
    (surface-add-pixel! pixel-set x y)))

(defgeneric remove-pixel! (pixel-set x y)
  (:method ((pixel-set pixel-set)
            (x integer)
            (y integer))
    (simple-remove-pixel! pixel-set x y))
  (:method ((pixel-set surfaced-pixel-set)
            (x integer)
            (y integer))
    (surface-remove-pixel! pixel-set x y)))

(defgeneric in-pixel-set? (pixel-set x y)
  (:method ((pixel-set pixel-set)
            (x integer)
            (y integer))
    (simple-in-pixel-set? pixel-set x y))
  (:method ((pixel-set surfaced-pixel-set)
            (x integer)
            (y integer))
    (in-pixel-set? (surfaced-pixel-set-content pixel-set)
                   x y)))

(defgeneric adjacent-to-pixel-set? (pixel-set x y)
  (:method ((pixel-set pixel-set)
            (x integer)
            (y integer))
    (simple-adjacent-to-pixel-set? pixel-set x y))
  (:method ((pixel-set surfaced-pixel-set)
            (x integer)
            (y integer))
    (is-surface-pixel? pixel-set x y)))

(defgeneric pixel-set-runs-table (pixel-set)
  (:method ((pixel-set pixel-set))
    (pixel-set-runs pixel-set))
  (:method ((pixel-set surfaced-pixel-set))
    (pixel-set-runs (surfaced-pixel-set-content pixel-set))))

(defgeneric pop-pixel! (pixel-set)
  (:method ((pixel-set pixel-set))
    (simple-pop-pixel! pixel-set))
  (:method ((pixel-set surfaced-pixel-set))
    (bind ((pixel (simple-pop-pixel! pixel-set))
           ((x y)
            pixel))
      (surface-remove-pixel! pixel-set x y))))

(defun add-pixel*! (pixel-set pixel)
  (add-pixel! pixel-set (first pixel)
              (second pixel)))

(defmacro do-pixel-set (pixel-set (x y)
                        &body
                          body)
  (bind ((g!runs (gensym "runs"))
         (g!run (gensym "run")))
    `(maphash (lambda (,y ,g!runs)
                (mapcar (lambda (,g!run)
                          (loop
                            :for ,x
                              :from (run-start ,g!run)
                                :to (run-end ,g!run)
                            :do
                               (progn
                                 ,@body)))
                        ,g!runs))
              (pixel-set-runs-table ,pixel-set))))

(defmacro do-surface (surfaced-pixel-set (x y)
                      &body
                        body)
  (bind ((g!pixel (gensym "pixel"))
         (g!_ (gensym "_")))
    `(maphash (lambda (,g!pixel ,g!_)
                (declare (ignore ,g!_))
                (bind (((,x ,y)
                        ,g!pixel))
                  ,@body))
              (surfaced-pixel-set-surface ,surfaced-pixel-set))))

;;; this can probably be optimized severely by doing a union operation on lists
;;; of runs instead of adding all the pixels one by one
;;; at least this optimization could work for non-surfaced pixel sets
(defun pixel-set-union! (pixel-set1 pixel-set2)
  (do-pixel-set pixel-set2 (x y)
    (add-pixel! pixel-set1 x y)))
