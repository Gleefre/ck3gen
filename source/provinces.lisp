(in-package #:ck3gen)

(defun components-remap (image components color->value
                         &key
                           (value-fn #'identity))
  (bind ((remapped (map)))
    (do-set (component components)
      (bind (((x y)
              (arb component)))
        (setf remapped
              (with remapped
                    component (funcall value-fn
                                       (@ color->value
                                          (pixel* image y x)))))))
    remapped))

(defun most-desirable-neighbor (component province score-fn)
  (bind ((most-desirable-x nil)
         (most-desirable-y nil)
         (score nil)
         (considered 1))
    (do-surface province (x y)
      (when (in-pixel-set? component x y)
        (bind ((nscore (funcall score-fn x y)))
          (if score
              (cond
                ((> nscore score)
                 (setf most-desirable-x x
                       most-desirable-y y
                       score nscore
                       considered 1))
                ((= nscore score)
                 (when (= (random (incf considered))
                          0)
                   (setf most-desirable-x x
                         most-desirable-y y))))
              (setf most-desirable-x x
                    most-desirable-y y
                    score nscore)))))
    (when most-desirable-x
      (values (list most-desirable-x most-desirable-y)
              score))))

(defun save-province (target-image csv-stream province
                      index color name-generator)
  (bind (((r g b)
          color))
    (format csv-stream "~A;~A;~A;~A;~A;x;~%"
            index r g b (funcall name-generator index province))
    (do-pixel-set province (x y)
      (setf (pixel* target-image y x)
            color))))

(defun place-province-centers (component province-number exclusion-radius
                               score-fn)
  (bind ((desired (make-heap #'>))
         (fingers (make-hash-table :test #'equal)))
    (do-pixel-set component (x y)
      (bind ((pixel (list x y)))
        (setf (gethash pixel fingers)
              (insert! desired (funcall score-fn x y)
                       pixel))))
    (loop
      :with provinces := (set)
      :repeat province-number
      :do
         (bind ((pixel (extract! desired))
                ((x y)
                 pixel))
           (remhash pixel fingers)
           (setf provinces
                 (with provinces (list x y)))
           (do-pixel-set (integer-ball pixel exclusion-radius)
               (x y)
             (bind ((pixel (list x y)))
               ([a]when (gethash pixel fingers)
                 (remhash pixel fingers)
                 (delete! desired it)))))
      :finally
         (return provinces))))

(defun province-number<-connected-components (connected-components area-mapping)
  (bind ((n 0))
    (do-set (connected-component connected-components)
      (incf n (ceiling (size connected-component)
                       (@ area-mapping connected-component))))
    n))

(defun grow-provinces (target-image csv-stream
                       component color-swatch
                       province-area exclusion-radius center-score growth-score
                       index-gen name-generator)
  (bind ((province-number (ceiling (size component)
                                   province-area))
         (province-surfaces (image (lambda (province-center)
                                     (bind ((surface
                                             (make-hash-table :test 'equal)))
                                       (setf (gethash province-center surface)
                                             t)
                                       surface))
                                   (place-province-centers component
                                                           province-number
                                                           exclusion-radius
                                                           center-score)))
         (province->color (convert 'map
                                   province-surfaces
                                   :key-fn #'identity
                                   :value-fn (lambda (_)
                                               (declare (ignore _))
                                               (funcall color-swatch)))))
    (until (zerop (size component))
      (do-set (surface province-surfaces)
        (bind ((most-desirable-x nil)
               (most-desirable-y nil)
               (score nil)
               (considered 1))
          (maphash (lambda (position _)
                     (declare (ignore _))
                     (bind (((x y)
                             position))
                       (if (in-pixel-set? component x y)
                           (bind ((nscore (funcall growth-score x y)))
                             (if score
                                 (cond
                                   ((> nscore score)
                                    (setf most-desirable-x x
                                          most-desirable-y y
                                          score nscore
                                          considered 1))
                                   ((= nscore score)
                                    (when (zerop (random (incf considered)))
                                      (setf most-desirable-x x
                                            most-desirable-y y))))
                                 (setf most-desirable-x x
                                       most-desirable-y y
                                       score nscore)))
                           (remhash position surface))))
                   surface)
          (when score
            (remhash (list most-desirable-x most-desirable-y)
                     surface)
            (remove-pixel! component most-desirable-x most-desirable-y)
            (do-neighbors most-desirable-x most-desirable-y (nx ny)
              (if (in-pixel-set? component nx ny)
                  (setf (gethash (list nx ny)
                                 surface)
                        t)
                  (remhash (list nx ny)
                           surface)))
            (bind ((color (@ province->color surface)))
              (setf (pixel* target-image most-desirable-y most-desirable-x)
                    color)))
          (when (zerop (hash-table-count surface))
            (bind ((color (@ province->color surface))
                   ((r g b)
                    color))
              (bind ((index (funcall index-gen)))
                (format csv-stream "~A;~A;~A;~A;~A;x;~%"
                        index r g b (funcall name-generator index color))
                (setf province-surfaces
                      (less province-surfaces surface))))))))))

(defun generate-provinces (landmass-file
                           target-png target-csv
                           color->center-score color->growth-score
                           color->province-radius
                           color->exclusion-ratio
                           name-generator)
  (bind ((landmass (read-png-file landmass-file))
         (connected-components (find-connected-components landmass))
         (comp->er (components-remap landmass connected-components
                                     (map-union color->province-radius
                                                color->exclusion-ratio
                                                #'*)))
         (comp->cs (components-remap landmass connected-components
                                     color->center-score))
         (comp->gs (components-remap landmass connected-components
                                     color->growth-score))
         (comp->pa (components-remap landmass connected-components
                                     color->province-radius
                                     :value-fn
                                     (lambda (radius)
                                       (ceiling (* (expt radius 2)
                                                   pi 2)))))
         (province-number
          (province-number<-connected-components connected-components comp->pa))
         (color-swatch (color-swatch province-number))
         (province-image (copy-image landmass)))
    (with-open-file (csv-stream target-csv
                                :direction :output
                                :if-exists :supersede
                                :if-does-not-exist :create)
      (format csv-stream "0;0;0;0;x;x;~%")
      (bind ((n 0))
        (do-set (connected-component connected-components)
          (grow-provinces province-image csv-stream connected-component
                          color-swatch
                          (@ comp->pa connected-component)
                          (@ comp->er connected-component)
                          (@ comp->cs connected-component)
                          (@ comp->gs connected-component)
                          (lambda ()
                            (incf n))
                          name-generator))))
    (write-png-file target-png province-image)))
